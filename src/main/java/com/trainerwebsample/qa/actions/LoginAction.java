package com.trainerwebsample.qa.actions;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import com.trainerwebsample.qa.pages.LoginPage;
import com.trainerwebsample.qa.utilities.Log;




public class LoginAction {

	static WebDriver driver;
	
	public static void TrainerLogin(WebDriver driver ,String Uname ,String Pswd) throws Exception{
		
		LoginPage login = new LoginPage(driver);
		
		login.TrainerUsername.isDisplayed();
		login.TrainerUsername.clear();
		login.TrainerUsername.sendKeys(Uname);	
		Thread.sleep(2000);
		Log.info("Entered Trainer valid username");
		
		login.TrainerPassword.isDisplayed();
		login.TrainerPassword.clear();
		login.TrainerPassword.sendKeys(Pswd);
		Thread.sleep(2000);
		Log.info("Entered Trainer valid Password");
		
		login.TrainerLoginBtn.isDisplayed();
		login.TrainerLoginBtn.isEnabled();	
		login.TrainerLoginBtn.click();	
		Thread.sleep(5000);
		
		Assert.assertEquals("Overview" ,"Overview" );
		Log.info("Click action performed on Loginbutton and succesfully Login into Application");
		
		Thread.sleep(5000);
	}
}

