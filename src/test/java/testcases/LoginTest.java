package testcases;


import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.trainerwebsample.qa.actions.LoginAction;
import com.trainerwebsample.qa.helpers.Browserhelper;
import com.trainerwebsample.qa.testdata.LoginDataProvider;
import com.trainerwebsample.qa.utilities.Log;



public class LoginTest {
	
	static WebDriver driver;
	
@BeforeClass
public void OpenBrowser() throws Exception {	
	PropertyConfigurator.configure("log4j.properties");
	driver = Browserhelper.openBrowser();}

@Test (dataProvider = "UserLogin" , dataProviderClass = LoginDataProvider.class)
public void TrainerLogin(String Username ,String Password) throws Exception{
	LoginAction.TrainerLogin(driver,Username,Password);
   // LogoutAction.UserLogOut(driver);
	}

@AfterClass
   public void teardown() throws Exception{	
	Log.info("Successfully validated Login functionality with valid ");
	driver.quit();
	}
}

